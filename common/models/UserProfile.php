<?php

namespace common\models;

use Yii;
//use yii\models\user;
use yii\helpers\Html;
use yii\behaviors\TimestampBehavior;
use yii\helpers\ArrayHelper;

use yii\web\NotFoundHttpException;
/**
 * This is the model class for table "user".
 *
 * @property integer $id
 * @property integer $default_engineer
 * @property integer $status
 * @property integer $role_id
 * @property string $name
 * @property string $username
 * @property string $email
 * @property string $auth_key
 * @property string $password_hash
 * @property string $password_reset_token
 * @property string $telephone
 * @property string $mobile
 * @property string $text_code
 * @property string $color
 * @property integer $include_in_diary_route_planning
 * @property string $address_line_1
 * @property string $address_line_2
 * @property string $address_line_3
 * @property string $town
 * @property string $postcode
 * @property string $plain_address
 * @property string $html_address
 * @property string $postcode_lat
 * @property string $postcode_lng
 * @property string $day_start_postcode
 * @property string $day_start_postcode_lat
 * @property string $day_start_postcode_lng
 * @property string $day_end_postcode
 * @property string $day_end_postcode_lat
 * @property string $day_end_postcode_lng
 * @property string $last_known_lat
 * @property string $last_known_lng
 * @property string $last_known_address
 * @property string $created_at
 * @property string $updated_at
 *
 * @property Customer[] $customers
 * @property Customer[] $customers0
 * @property DocsMans[] $docsMans
 * @property Notify[] $notifies
 * @property Product[] $products
 * @property Product[] $products0
 * @property Service[] $services
 * @property Service[] $services0
 * @property ServiceInvoice[] $serviceInvoices
 * @property ServiceInvoice[] $serviceInvoices0
 * @property ServiceLog[] $serviceLogs
 * @property ServiceSpares[] $serviceSpares
 * @property ServiceSpares[] $serviceSpares0
 * @property UserRole $role
 * @property UserDiary[] $userDiaries
 * @property UserDiary[] $userDiaries0
 * @property UserDiary[] $userDiaries1
 * @property UserDiaryLog[] $userDiaryLogs
 */
class UserProfile extends \yii\db\ActiveRecord
{


    const STATUS_DELETED = 0;
    const STATUS_ACTIVE = 10;
    public $password;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'user';
    }



        /**
         * Finds the UserDiary model based on its primary key value.
         * If the model is not found, a 404 HTTP exception will be thrown.
         * @param integer $id
         * @return UserDiary the loaded model
         * @throws NotFoundHttpException if the model cannot be found
         */
        public static function findModel($id)
        {
            if (($model = UserProfile::findOne($id)) !== null) {
                return $model;
            } else {
                throw new NotFoundHttpException('The requested page does not exist.');
            }
        }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['default_engineer', 'status', 'role_id', 'include_in_diary_route_planning'], 'integer'],
            [['text_code','status', 'role_id', 'name', 'email', 'color', 'address_line_1', 'town', 'postcode', 'postcode_lat', 'postcode_lng', 'day_start_postcode', 'day_start_postcode_lat', 'day_start_postcode_lng', 'day_end_postcode', 'day_end_postcode_lat', 'day_end_postcode_lng'], 'required'],

            ['password', 'required', 'on' => 'create'],
            ['username', 'required', 'on' => 'update'],


            [['name', 'address_line_1', 'address_line_2', 'address_line_3', 'town', 'postcode', 'plain_address', 'html_address', 'last_known_address'], 'string'],
            [['created_at', 'updated_at'], 'safe'],
            [['password','username', 'email', 'password_hash', 'password_reset_token'], 'string', 'max' => 255],
            [['auth_key', 'telephone', 'mobile', 'text_code', 'color', 'postcode_lat', 'postcode_lng', 'day_start_postcode', 'day_start_postcode_lat', 'day_start_postcode_lng', 'day_end_postcode', 'day_end_postcode_lat', 'day_end_postcode_lng', 'last_known_lat', 'last_known_lng'], 'string', 'max' => 32],

            [['username'], 'unique'],
            [['email'], 'unique'],
            [['password_reset_token'], 'unique'],
            [['role_id'], 'exist', 'skipOnError' => true, 'targetClass' => UserRole::className(), 'targetAttribute' => ['role_id' => 'id']],
        ];
    }





    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => Handyfunctions::convertToMysqlDateTime(time()),

            ],
        ];
    }



    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'default_engineer' => 'Default Engineer',
            'status' => 'Status',
            'role_id' => 'Role ID',
            'name' => 'Name',
            'username' => 'Username',
            'email' => 'Email',
            'auth_key' => 'Auth Key',
            'password_hash' => 'Password Hash',
            'password_reset_token' => 'Password Reset Token',
            'telephone' => 'Telephone',
            'mobile' => 'Mobile',
            'text_code' => 'Text Code',
            'color' => 'Color',
            'include_in_diary_route_planning' => 'Include In Diary Route Planning',
            'address_line_1' => 'Address Line 1',
            'address_line_2' => 'Address Line 2',
            'address_line_3' => 'Address Line 3',
            'town' => 'Town',
            'postcode' => 'Postcode',
            'plain_address' => 'Plain Address',
            'html_address' => 'Html Address',
            'postcode_lat' => 'Postcode Lat',
            'postcode_lng' => 'Postcode Lng',
            'day_start_postcode' => 'Day Start Postcode',
            'day_start_postcode_lat' => 'Day Start Postcode Lat',
            'day_start_postcode_lng' => 'Day Start Postcode Lng',
            'day_end_postcode' => 'Day End Postcode',
            'day_end_postcode_lat' => 'Day End Postcode Lat',
            'day_end_postcode_lng' => 'Day End Postcode Lng',
            'last_known_lat' => 'Last Known Lat',
            'last_known_lng' => 'Last Known Lng',
            'last_known_address'=>'Last Known address',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCustomers()
    {
        return $this->hasMany(Customer::className(), ['created_by_user_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCustomers0()
    {
        return $this->hasMany(Customer::className(), ['updated_by_user_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDocsMans()
    {
        return $this->hasMany(DocsMans::className(), ['created_by_user_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getNotifies()
    {
        return $this->hasMany(Notify::className(), ['created_by_user_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProducts()
    {
        return $this->hasMany(Product::className(), ['created_by_user_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProducts0()
    {
        return $this->hasMany(Product::className(), ['updated_by_user_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getServices()
    {
        return $this->hasMany(Service::className(), ['created_by_user_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getServices0()
    {
        return $this->hasMany(Service::className(), ['updated_by_user_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getServiceInvoices()
    {
        return $this->hasMany(ServiceInvoice::className(), ['created_by_user_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getServiceInvoices0()
    {
        return $this->hasMany(ServiceInvoice::className(), ['updated_by_user_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getServiceLogs()
    {
        return $this->hasMany(ServiceLog::className(), ['user_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getServiceSpares()
    {
        return $this->hasMany(ServiceSpares::className(), ['created_by_user_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getServiceSpares0()
    {
        return $this->hasMany(ServiceSpares::className(), ['updated_by_user_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRole()
    {
        return $this->hasOne(UserRole::className(), ['id' => 'role_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserDiaries()
    {
        return $this->hasMany(UserDiary::className(), ['created_by_user_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserDiaries0()
    {
        return $this->hasMany(UserDiary::className(), ['updated_by_user_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserDiaries1()
    {
        return $this->hasMany(UserDiary::className(), ['user_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserDiaryLogs()
    {
        return $this->hasMany(UserDiaryLog::className(), ['created_by_user_id' => 'id']);
    }


    public function getViewLink()
    {
        return Html::a($this->name,['/user-profile/view','id'=>$this->id]);
    }



    public static function getUser($id)
    {
        if (($user = UserProfile::findOne($id)) !== null) {
            return $user;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }


    public static function findByTextCode($text_code)
    {
        $user=UserProfile::find()->where(['text_code'=>$text_code])->one();

        if ($user)
            return $user;
        else
            throw new NotFoundHttpException('The User '.$text_code.' is not found in database. Please contact support.');

    }



    public static function getDefaultEngineer()
    {
        $engineer=UserProfile::find()->where(['default_engineer'=>'1'])->one();

        if ($engineer)
            return $engineer;
        else
            throw new NotFoundHttpException('There is no default engineer Set. Please set the default engineer first .');

    }



    public static function getUserStatusesAsArray()
    {
        ///When updating this also update the user class
        ///
        ///const STATUS_DELETED = 0;
        ///const STATUS_ACTIVE = 10;
        return ['10'=>'Active', '0'=>'Deleted'];
    }


    public function getUserStatus()
    {
        $statuses=self::getUserStatusesAsArray();
        return $statuses[$this->status];
    }///end of     public function getRole()


    public function isActive()
    {
        if ($this->status==10)
            return true;
        else
            return false;

    }


    public static function signUpNewUser($model)
    {
        $user = new User();


        $user->username = $model->email;
        $user->email = $model->email;
        $user->role_id  = $model->role_id;
        $user->name= $model->name;
        $user->text_code= $model->text_code;

        $user->status=$model->status;
        $user->setPassword($model->password);
        $user->generateAuthKey();
        return $user->save() ? $user : null;
    }






    public static function getUsers($role_text_code=false, $drop_down=true, $active=true, $ids=false, $names=false)
    {
        $query = UserProfile::find();

        if ($active)
            $query->andFilterWhere(['status'=>'10']);

        if ($role_text_code)
        {
            $role_id=UserRole::findByTextCode($role_text_code)->id;
            $query->andFilterWhere(['role_id'=>$role_id]);
            $query->orFilterWhere(['role_id'=>UserRole::findByTextCode('ADMIN')->id]);

            if ($role_text_code=='ENGINEER')
            {
                $query->andFilterWhere(['include_in_diary_route_planning'=>true]);
            }

        }
        $query->orderBy(['name'=>SORT_ASC]);


        $models=$query->all();

        $all_key_value_array = ArrayHelper::map($models, 'id', 'name');

        if ($drop_down)
            return $all_key_value_array;

        if ($ids)
            return array_keys($all_key_value_array);

        if ($names)
            return array_values($all_key_value_array);

        return $models;

    }//end of  public static function getUsers($role_text_code=false, $drop_down=true, $active=true, $ids=false, $names=false)


    public function sendMobileLinkEmail()
    {
        $msg='';
        $msg.='Hello '.$this->name;
        $msg.='<br><br>';

        $msg.='You can access your mobile diary using following link';
        $msg.='<br><br>';
        $msg.='<a class="btn btn-primary" href="'.Handyfunctions::getFrontEndUrl().'">Mobile Diary Access Link</a>';

        $msg.='<br><br>';
        $msg.='If you are a new user or have forgotten password, please use following link';
        $msg.='<br><br>';
        $msg.='<a class="btn btn-primary" href="'.Handyfunctions::getFrontEndUrl().'site/request-password-reset">Password reset Link</a>';



        $subject=SystemConfig::getParam('COMPANY_NAME').' Mobile Diary';

        $mailTo=$this->email;

        return Handyfunctions::sendEmail($mailTo, $subject, $msg);










    }///end of public function sendMobileLinkEmail()




}
