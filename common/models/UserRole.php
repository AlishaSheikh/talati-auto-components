<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "user_role".
 *
 * @property integer $id
 * @property string $name
 * @property string $text_code
 * @property string $info
 *
 * @property User[] $users
 */
class UserRole extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'user_role';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'text_code'], 'required'],
            [['info'], 'string'],
            [['name', 'text_code'], 'string', 'max' => 64],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'text_code' => 'Text Code',
            'info' => 'Info',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUsers()
    {
        return $this->hasMany(User::className(), ['role' => 'id']);
    }






    public static function findByTextCode($text_code)
    {
        $user_role=UserRole::find()->where(['text_code'=>$text_code])->one();

        if ($user_role)
            return $user_role;
        else
            throw new NotFoundHttpException('There is no default engineer Set. Please set the default engineer first .');

    }
}
